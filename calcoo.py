#!/usr/bin/python3
# -*- coding: utf-8 -*-


import sys


class Calc():
    def add(self, op1, op2):
        return op1 + op2

    def sub(self, op1, op2):
        return op1 - op2

if __name__ == "__main__":
    try:
        op1 = float(sys.argv[1])
        op2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: first and third arguments should be numbers")

    objeto = Calc()

    if sys.argv[2] == "+":
        print(objeto.add(op1, op2))
    elif sys.argv[2] == "-":
        print(objeto.sub(op1, op2))
    else:
        sys.exit('Operand should be + or -')
