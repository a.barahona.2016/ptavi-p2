#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from calcoo import Calc


class CalcChild (Calc):
    def mul(self, op1, op2):
        return op1 * op2

    def div(self, op1, op2):
        try:
            return op1 / op2
        except ZeroDivisionError:
            print("Division by zero is not allowed")
            raise ValueError('Error')

if __name__ == "__main__":
    try:
        op1 = float(sys.argv[1])
        op2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: first and third arguments should be numbers")

    objeto = CalcChild()

    if sys.argv[2] == "+":
        print(objeto.add(op1, op2))
    elif sys.argv[2] == "-":
        print(objeto.sub(op1, op2))
    elif sys.argv[2] == "*":
        print(objeto.mul(op1, op2))
    elif sys.argv[2] == "/":
        print(objeto.div(op1, op2))
    else:
        sys.exit('Operand should be + or - or * or /')
